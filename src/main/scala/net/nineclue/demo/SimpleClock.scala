package net.nineclue.demo

import javafx.application.{Application, Platform}
import javafx.stage.{Stage, StageStyle, Modality}
import javafx.fxml.{FXML, FXMLLoader}
import java.net.URL
import javafx.scene.layout.{AnchorPane, VBox}
import javafx.scene.Scene
import javafx.scene.control.{Label, Button}
import javafx.scene.image.{Image, ImageView}
import java.util.{Timer, TimerTask}
import java.util.{Calendar, GregorianCalendar}
import javafx.scene.text.{Text, Font}
import javafx.geometry.Pos
import javafx.event.{EventHandler, ActionEvent}
import javafx.scene.paint.Color

object SimpleClock {
  val circle = new Image(getClass.getResourceAsStream("/circle.png"))
  val myFont = Font.loadFont(getClass.getResourceAsStream("/NanumPen.ttf"), 48)
  def main(as:Array[String]) = Application.launch(classOf[SimpleClock], as:_*)
}

class SimpleClock extends Application {
  @FXML var time:Label = null
  @FXML var aboutButton:Button = null
  val clock = new Timer()
  def timeString = {
    val c = new GregorianCalendar()
    s"""${if (c.get(Calendar.AM_PM) == 0) "AM" else "PM"} ${c.get(Calendar.HOUR)} : ${c.get(Calendar.MINUTE)} : ${c.get(Calendar.SECOND)}"""
  }

  def start(ps:Stage) = {
    val loader = new FXMLLoader()
    loader.setController(this)
    val root = loader.load(getClass.getResourceAsStream("/clock.fxml")).asInstanceOf[AnchorPane]
    val scene = new Scene(root)
    ps.setScene(scene)

    aboutButton.setGraphic(new ImageView(SimpleClock.circle))
    clock.scheduleAtFixedRate(new TimerTask() {
      def run = Platform.runLater(new Runnable() {
        def run = time.setText(timeString)
      })
    }, 0, 1000)

    ps.show
  }

  def showAbout = {
    val about = new Stage()
    val root = new VBox()
    root.setAlignment(Pos.CENTER)
    root.setPrefSize(300, 200)
    root.setStyle("-fx-background-color: white;")
    root.setSpacing(20)

    val text = new Text(100, 100, "시계데모")
    text.setFont(SimpleClock.myFont)
    text.setFill(Color.LIGHTGREEN)

    val close = new Button()
    close.setGraphic(new ImageView(SimpleClock.circle))
    close.setOnAction(new EventHandler[ActionEvent]() {
      def handle(e:ActionEvent) = about.close
    })

    root.getChildren.addAll(text, close)

    val aboutScene = new Scene(root)
    about.setScene(aboutScene)
    // about.setAlwaysOnTop(true)
    about.initModality(Modality.APPLICATION_MODAL)
    about.initStyle(StageStyle.UNDECORATED)
    about.show
  }
}
